﻿$(document).ready(function () {
    
	$.connection.hub.start().done(function () {
	    $("#Welcome_Connection").html("Connection Established");
	});

	$.connection.lobbyHub.client.lobbyGotoLobby = Welcome.GotoLobby;
	$.connection.lobbyHub.client.lobbyErrorJoiningLobby = Welcome.ErrorJoiningLobby;
	$.connection.lobbyHub.client.lobbyUpdateLobbyUserList = Lobby.UpdateLobbyUserList;
	$.connection.lobbyHub.client.lobbyUpdateSpectatorCount = Lobby.UpdateSpectatorCount;
	$.connection.lobbyHub.client.lobbyKickSpectator = Lobby.KickSpectator;
    $.connection.lobbyHub.client.lobbyCloseLobby = Lobby.LobbyClose;

    $.connection.gameHub.client.gameShowCurrentCards = Game.ShowCurrentCards;
    $.connection.gameHub.client.gameShowCurrentQuestionLeader = Game.ShowCurrentQuestionLeader;
    $.connection.gameHub.client.gameShowCurrentQuestionPlayer = Game.ShowCurrentQuestionPlayer;
    $.connection.gameHub.client.gameRecievedAnswer = Game.RecievedAnswer;
    $.connection.gameHub.client.gameRecievedAllAnswers = Game.RecievedAllAnswers;
    $.connection.gameHub.client.gameRecievedScores = Game.RecievedScores;
    $.connection.gameHub.client.lobbyGotoLobby = Welcome.GotoLobby;

    $.connection.gameHub.client.gameShowCurrentQuestionSpectator = Spectator.ShowCurrentQuestion;
    $.connection.gameHub.client.gameRecievedScoresSpectator = Spectator.RecievedScores
    $.connection.gameHub.client.gameRecievedAllAnswersSpectator = Spectator.RecievedAllAnswers;
    $.connection.gameHub.client.gameRecievedAnswerSpectator = Spectator.RecievedAnswer;
});