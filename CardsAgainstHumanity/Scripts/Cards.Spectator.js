﻿var Spectator = {
    ShowCurrentQuestion: function(questionText) {
        $("#Page_Lobby").css("display", "none");
        $("#Page_Spectator").css("display", "block");

        $("#Spectator_CurrentQuestion").text(questionText);
        $("#Spectator_AnswerList").empty();
    },

    RecievedScores: function(scores) {
        $("#Spectator_ScoreList").empty();
        $("#Spectator_ScoreList").append("<tr><th style=\"text-align: center;\">Player</th><th style=\"text-align: center;\">Score</th></tr>");

        scores.forEach(function (element) {
            $("#Spectator_ScoreList").append("<tr><td>" + element.Name + "</td><td>" + element.NumberOfPoints + "</td></tr>");
        });
    },

    RecievedAnswer: function () {
        $("#Spectator_AnswerList").append("<li>.....</li>");
    },

    RecievedAllAnswers: function (answers) {
        $("#Spectator_AnswerList").empty();
        $(answers).each(function (i, element) {

            $("#Spectator_AnswerList").append("<li>" + element + "</li>");
        });
    }
};