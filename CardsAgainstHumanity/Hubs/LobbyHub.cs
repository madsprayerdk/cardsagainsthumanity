﻿// ReSharper disable UnusedMember.Global
// ReSharper disable ClassNeverInstantiated.Global
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CAH.Model;
using Microsoft.AspNet.SignalR;

namespace CardsAgainstHumanity.Hubs
{
    public class LobbyHub : Hub
    {
        public static readonly List<Lobby> Lobbies = new List<Lobby>();

        public void CreateNewLobby(string userName, bool allowSpectators)
        {
            if (Lobbies.Any(x => x.Users.Any(y => y.ConnectionId == Context.ConnectionId)))
            {
                RemoveUserFromLobby(Context.ConnectionId);
            }

            string lobbyCode;
            do
            {
                var random = new Random();
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                lobbyCode = new string(
                Enumerable.Repeat(chars, 6)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            } while (Lobbies.Any(x => x.Code == lobbyCode));

            var lobby = new Lobby
            {
                Code = lobbyCode,
                AllowSpectators = allowSpectators
            };

            lobby.Users.Add(new User { ConnectionId = Context.ConnectionId, Name = userName });

            Lobbies.Add(lobby);

            Clients.Caller.lobbyGotoLobby(lobbyCode);
            Clients.Caller.lobbyUpdateLobbyUserList(lobby.Users.Select(x => x.Name));

            if(lobby.AllowSpectators)
                Clients.Caller.lobbyUpdateSpectatorCount(lobby.Spectators.Count);
        }

        public void JoinLobby(string name, string lobbyCode, bool spectator)
        {
            var code = lobbyCode.ToUpper();

            if (Lobbies.All(x => x.Code != code))
            {
                Clients.Caller.lobbyErrorJoiningLobby("Can't find lobby with specified code.");
                return;
            }

            var lobby = Lobbies.Single(x => x.Code == code);

            if (lobby.ActiveGame)
            {
                Clients.Caller.lobbyErrorJoiningLobby("Can't join a game in progress");
                return;
            }

            // If user are spectator, but lobby doesn't allow spectators. Reject the user.
            if (spectator && !lobby.AllowSpectators)
            {
                Clients.Caller.lobbyErrorJoiningLobby("Lobby does not allow spectators");
                return;
            }

            if(spectator)
                lobby.Spectators.Add(Context.ConnectionId);
            else
                lobby.Users.Add(new User { ConnectionId = Context.ConnectionId, Name = name });

            Clients.Caller.lobbyGotoLobby(code, spectator);

            foreach (var user in lobby.Users)
            {
                Clients.Client(user.ConnectionId).lobbyUpdateLobbyUserList(lobby.Users.Select(x => x.Name));

                if (lobby.AllowSpectators)
                    Clients.Client(user.ConnectionId).lobbyUpdateSpectatorCount(lobby.Spectators.Count);
            }

            if (!lobby.AllowSpectators)
                return;

            foreach (var spec in lobby.Spectators)
            {
                Clients.Client(spec).lobbyUpdateLobbyUserList(lobby.Users.Select(x => x.Name));
                Clients.Client(spec).lobbyUpdateSpectatorCount(lobby.Spectators.Count);
            }
        }

        public void LeaveLobby()
        {
            RemoveUserFromLobby(Context.ConnectionId);
        }

        private void RemoveUserFromLobby(string connectionId)
        {
            var lobby = Lobbies.SingleOrDefault(x => x.Users.Any(y => y.ConnectionId == connectionId) || x.Spectators.Any(y => y.Equals(connectionId)));

            if (lobby == null)
                return;

            if (lobby.Users.Any(x => x.ConnectionId == connectionId))
                lobby.Users.Remove(lobby.Users.Single(x => x.ConnectionId == connectionId));
            else
                lobby.Spectators.Remove(connectionId);

            if (lobby.Users.Count == 0)
            {
                foreach (var spectator in lobby.Spectators)
                {
                    Clients.Client(spectator).lobbyKickSpectator();
                    Clients.Client(spectator).lobbyCloseLobby("Lobby is closed.");
                }

                Lobbies.Remove(lobby);
            }
            else
            {
                foreach (var user in lobby.Users)
                {
                    Clients.Client(user.ConnectionId).gameRecievedScores(lobby.Users.Select(x => new { x.Name, x.NumberOfPoints }));
                    Clients.Client(user.ConnectionId).lobbyUpdateLobbyUserList(lobby.Users.Select(x => x.Name));

                    if (lobby.AllowSpectators)
                        Clients.Client(user.ConnectionId).lobbyUpdateSpectatorCount(lobby.Spectators.Count);
                }
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            RemoveUserFromLobby(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }
    }
}